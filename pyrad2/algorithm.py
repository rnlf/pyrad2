from enum import Enum

class Algorithm(Enum):
  """Synthesis algorithms supported by RAD"""
  FM      = 0
  AM      = 1
  FM_FM   = 2
  AM_FM   = 3
  FM_AM   = 4
  AM_AM   = 5
  DUAL_AM = 6
  MIDI    = 7
