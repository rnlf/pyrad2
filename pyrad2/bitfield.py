# Not super fast, but it works well enough for our purposes
def logical_right_shift_bytes(data, bit_count):
  """Shifts the content of the bytes or bytearray in data by bit_count bits to the right without sign expansion.
     Truncates the return value by bit_count // 8 bytes from the left.
     This means it returns b'' if bit_count is bigger than number of bits in array."""

  if bit_count < 0:
    raise ValueError()

  byte_count = bit_count // 8
  bit_count = bit_count % 8
  tmp = bytearray(data[0:len(data)-byte_count])

  for i in range(len(tmp)-1,-1,-1):
    tmp[i] = (tmp[i] >> bit_count) | (((tmp[i-1] << (8-bit_count)) & 0xff) if i > 0 else 0)

  return bytes(tmp)


# Not super fast, but it works well enough for our purposes
def logical_left_shift_bytes(data, bit_count, **kwargs):
  """Shifts the content of the bytes or bytearray in data by bit_count bits to the left.
     The size of the return value will be increased by bit_count, rounded up to full bytes,
     unless argument truncate is given, in which case the result will be truncated to that length.
     truncate must not be larger than the length of the resulting value.
     truncate=-1 will keep the length of the original input."""

  if bit_count < 0:
    raise ValueError()

  left_byte_count = (bit_count % 8 + 7) // 8
  right_byte_count = bit_count // 8
  bit_count = bit_count % 8
  tmp = bytearray(b'\x00'*left_byte_count + data)

  for i in range(len(tmp)):
    tmp[i] = ((tmp[i] << bit_count) & 0xff) | (((tmp[i+1] >> (8-bit_count))) if i < len(tmp) - 1 else 0)

  tmp = bytes(tmp) + b'\0' * right_byte_count

  if 'truncate' in kwargs:
    trunc = kwargs['truncate'] if kwargs['truncate'] != -1 else len(data)
    tmp = tmp[len(tmp)-trunc:]

  return tmp



class Bitfield:
  def __init__(self, length, *fields):
    self.length = length
    self.fields = fields

  def decode(self, input):
    data = input.read(self.length)
    if len(data) != self.length:
      raise EOFError()

    result = {}

    for f in self.fields:
      offset = f['offset']
      length = f['length']
      endian = f['endian'] if 'endian' in f else 'little'
      signed = f['signed'] if 'signed' in f else False

      start_index = offset // 8
      len_bytes = (length + 7) // 8
      end_index   = start_index + len_bytes

      tmp = int.from_bytes(data[start_index:end_index], "little")
      tmp = ((tmp >> offset % 8) & ((1 << length)-1)).to_bytes(len_bytes, "little")
      print(tmp)
      tmp = int.from_bytes(tmp, byteorder=endian)

      if signed and tmp & (1 << (length-1)) != 0:
        tmp = -(tmp ^ (1 << (length))) - 1

      result[f['name']] = tmp

    return result

  def encode(self, output, **fields):
    pass
