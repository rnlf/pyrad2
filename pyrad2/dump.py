#!/usr/bin/env python3

from . import parser
from .instrument import FMInstrument
from sys import argv, stdout

tune = parser.parse(open(argv[1], "rb"))


def dump_description(tune):
  print("Description:")
  print("------------------------------")
  print(tune.description if tune.description else "<no description>")
  print()
  print()


def dump_instruments(tune):
  print("Instruments:")
  print("------------------------------")
  for n, i in tune.instruments.items():
    dump_instrument(n, i)


def dump_instrument(n, i):
  print("{:02x}: {}".format(n, i.name if i.name else "<unnamed>"))
  if isinstance(i, FMInstrument):
    dump_fm_instrument(i)
  else:
    dump_midi_instrument(i)
  if i.riff:
    print("  Instrument Riff:")
    dump_pattern(i.riff)


def dump_fm_instrument(i):
  print("  Type:            FM")
  print("  Algorithm:       " + str(i.algorithm))
  print("  Panning Ch 1+2:  {}".format(i.panning12))
  print("  Panning Ch 3+4:  {}".format(i.panning34))
  print("  Feedback Ch 1+2: {}".format(i.feedback12))
  print("  Feedback Ch 3+4: {}".format(i.feedback34))
  print("  Detune:          {}".format(i.detune))
  print("  Riff speed:      {}".format(i.riff_speed))
  print("  Volume:          {}".format(i.volume))
  print("  Operators:")
  for oi, op in enumerate(i.operators):
    print("    Operator {}".format(oi))
    print("      Tremolo:     {}".format(op.tremolo))
    print("      Vibrato:     {}".format(op.vibrato))
    print("      Sustain:     {}".format(op.sustain))
    print("      Scale Rate:  {}".format(op.scale_rate))
    print("      Multiplier:  {}".format(op.multiplier))
    print("      Scale Level: {}".format(op.scale_level))
    print("      Volume:      {}".format(op.volume))
    print("      ADSR:        A={}, D={}, S={}, R={}".format(
      op.adsr_attack,
      op.adsr_decay,
      op.adsr_sustain,
      op.adsr_release))
    print("      Waveform:    {}".format(op.waveform))


def dump_midi_instrument(i):
    print("  Type: MIDI")

def dump_order_list(tune):
  print("\n\nOrder list:\n  " + " ".join(["{:02x}".format(x) for x in tune.order]))

def dump_patterns(tune):
  print("\n\nPatterns:")
  for i, p in tune.patterns.items():
    print("  Pattern {:02x}".format(i))
    dump_pattern(p)


def dump_pattern(p, channels = range(9)):
  for i, l in p.lines.items():
    stdout.write("    {:02x}:   ".format(i))
    for c in channels:
      if c in l.channels:
        dump_line_channel(l.channels[c])
      else:
        stdout.write("--- ----")

      stdout.write(" | ")

    stdout.write("\n")


def note_field(c):
  if c.note == None:
    return "---"
  elif c.note == 15:
    return "OFF"
  else:
    NOTE_NAMES=['C#', 'D-', 'D#', 'E-', 'F-', 'F#', 'G-', 'G#', 'A-', 'A#', 'B-', 'C-']
    return "{}{}".format(NOTE_NAMES[c.note-1], c.octave)

def dump_line_channel(c):
  EFFECT_NAMES=[str(x) for x in range(10)] + [chr(x) for x in range(ord('A'), ord('V')+1)]
  stdout.write( "{}{:2}{:1}{}".format(
    note_field(c),
    "{:2X}".format(c.instrument)        if c.instrument       != None else " -",
    EFFECT_NAMES[c.effect]              if c.effect           != None else "-",
    "{:02X}".format(c.effect_parameter) if c.effect_parameter != None else "--"
  ))

def dump_riffs(tune):
  print("\n\nRiffs:")
  for (t,c), p in tune.riffs.items():
    print("  Riff {:01x}{:01X}".format(t,c))
    dump_pattern(p, [c-1])

dump_description(tune)
dump_instruments(tune)
dump_order_list(tune)
dump_patterns(tune)
dump_riffs(tune)
