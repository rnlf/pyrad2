#!/usr/bin/env python3

from pyrad2 import parser, instrument, algorithm
from enum import Enum, auto


class Feature(Enum):
  EFFECT_SLIDE_NOTE_DOWN                    = auto()
  EFFECT_SLIDE_NOTE_UP                      = auto()
  EFFECT_SLIDE_TO_NOTE                      = auto()
  EFFECT_COMBINED_SLIDE_TO_NODE_AND_VOLUME  = auto()
  EFFECT_VOLUME_SLIDE                       = auto()
  EFFECT_SET_VOLUME                         = auto()
  EFFECT_BREAK                              = auto()
  EFFECT_BREAK_TO_LINE                      = auto()
  EFFECT_SET_SPEED                          = auto()
  EFFECT_IGNORE_TRANSPOSE                   = auto()
  EFFECT_MULTIPLIER                         = auto()
  EFFECT_RIFF                               = auto()
  EFFECT_TRANSPOSE_RIFF                     = auto()
  EFFECT_FEEDBACK                           = auto()
  EFFECT_VOLUME                             = auto()
  RIFF                                      = auto()
  INSTRUMENT_RIFF                           = auto()
  INSTRUMENT_DETUNE                         = auto()
  ORDER_LIST_JUMP                           = auto()
  INSTRUMENT_4OP_ALGORITHM                  = auto()
  INSTRUMENT_2OP_ALGORITHM                  = auto()



def check_break_command_param(chan, features):
  if chan.effect_parameter != 0:
    features[Feature.EFFECT_BREAK_TO_LINE] = True


EFFECT_NAMES = {
  1: { 'name': Feature.EFFECT_SLIDE_NOTE_DOWN },
  2: { 'name': Feature.EFFECT_SLIDE_NOTE_UP },
  3: { 'name': Feature.EFFECT_SLIDE_TO_NOTE },
  5: { 'name': Feature.EFFECT_COMBINED_SLIDE_TO_NODE_AND_VOLUME },
 10: { 'name': Feature.EFFECT_VOLUME_SLIDE },
 12: { 'name': Feature.EFFECT_SET_VOLUME },
 13: { 'name': Feature.EFFECT_BREAK, 'extra': check_break_command_param },
 15: { 'name': Feature.EFFECT_SET_SPEED },
 18: { 'name': Feature.EFFECT_IGNORE_TRANSPOSE },
 22: { 'name': Feature.EFFECT_MULTIPLIER },
 27: { 'name': Feature.EFFECT_RIFF },
 29: { 'name': Feature.EFFECT_TRANSPOSE_RIFF },
 30: { 'name': Feature.EFFECT_FEEDBACK },
 31: { 'name': Feature.EFFECT_VOLUME }
}

def find_used_effects(patterns, features):
  for pat in patterns:
    for line in pat.lines.values():
      for chan in line.channels.values():
        if chan.effect != None:
          eff = EFFECT_NAMES[chan.effect]
          features[eff['name']] = True
          if 'extra' in eff:
            eff['extra'](chan, features)


def find_effects(tune, features):
  find_used_effects(tune.patterns.values(), features)
  find_used_effects(tune.riffs.values(), features)

  return features


def uses_instrument_riffs(tune):
  for i in tune.instruments.values():
    if i.riff:
      return True

  return False


def uses_detune(tune):
  for i in tune.instruments.values():
    if isinstance(i, instrument.FMInstrument):
      if i.detune != 0:
        return True

  return False


ALGORITHM_OPS = {
  algorithm.Algorithm.FM: 2,
  algorithm.Algorithm.AM: 2,
  algorithm.Algorithm.FM_FM: 4,
  algorithm.Algorithm.AM_FM: 4,
  algorithm.Algorithm.FM_AM: 4,
  algorithm.Algorithm.AM_AM: 4,
  algorithm.Algorithm.DUAL_AM: 4,
  algorithm.Algorithm.MIDI: 0
};


def uses_4op(tune):
  for i in tune.instruments.values():
    if isinstance(i, instrument.FMInstrument):
      if ALGORITHM_OPS[i.algorithm] == 4:
        return True

  return False


def uses_2op(tune):
  for i in tune.instruments.values():
    if isinstance(i, instrument.FMInstrument):
      if ALGORITHM_OPS[i.algorithm] == 2:
        return True

  return False


def uses_order_list_jump(tune):
  for o in tune.order:
    if o >= 0x80:
      return True

  return False


def uses_riffs(tune):
  return len(tune.riffs) > 0


def detect_features(tune):
  features = {
    f: False for f in Feature
  }

  find_effects(tune, features)
  features[Feature.RIFF] = uses_riffs(tune)
  features[Feature.INSTRUMENT_RIFF] = uses_instrument_riffs(tune)
  features[Feature.INSTRUMENT_DETUNE] = uses_detune(tune)
  features[Feature.INSTRUMENT_2OP_ALGORITHM] = uses_2op(tune)
  features[Feature.INSTRUMENT_4OP_ALGORITHM] = uses_4op(tune)
  features[Feature.ORDER_LIST_JUMP] = uses_order_list_jump(tune)

  return features
