from instrument import FMInstrument
from consts import *
import re

def generate(output, tune):
  output.write(b"RAD by REALiTY!!!")

  gen_rad_file(output, tune)


def gen_rad_file(output, tune):
  timing = tune.speed

  if tune.slow:
    timing = timing | SLOW_TUNE_BIT

  if tune.bpm != 125:
    timing = timing | BPM_PRESENT_BIT

  output.write(bytes([timing]))
    
  if tune.bpm != 125:
    output.write(tune.bpm.to_bytes(2, 'little'))

  gen_description(output, tune)
  gen_instruments(output, tune)
  gen_orderlist(output, tune)
  gen_patterns(output, tune)


def gen_description(output, tune):
  def rle_spaces(m):
    ret = b""
    s = len(m.group(0))
    while s > 1:
      count = s if s <= 0x1f else 0x1f
      ret = ret + count.to_bytes(1, "little")
      s = s - count

    if s == 1:
      ret += b' '

    return ret

  space_re = re.compile(b'  +')
  nl_re = re.compile(b'\n')

  descr = tune.description.encode()
  descr = re.sub(space_re, rle_spaces, descr)
  descr = re.sub(nl_re, b'\x01', descr)

  output.write(descr)
  output.write(b'\0')


def gen_instruments(output, tune):
  for n, instr in tune.instruments.items():
    output.write(n.to_bytes(1, "little"))
    gen_instrument(output, instr)

  output.write(b'\0')


def gen_instrument(output, instrument):
  output.write(instrument.name.encode())

  if isinstance(instrument, FMInstrument):
    gen_fm_instrument(output, instrument)

  else:
    gen_midi_instrument(output, instrument)

  if instrument.riff:
    # TODO
    output.write(int(0).to_bytes(2, "little"))

def gen_fm_instrument(output, instrument):
  byte0 = instrument.algorithm.value | (instrument.panning34 << 5) | (instrument.panning12 << 3)

  if instrument.riff:
    byte0 = byte0 | HAS_RIFF_BIT

  output.write(byte0.to_bytes(1, 'little'))
  output.write((instrument.feedback12 | (instrument.feedback34 << 4)).to_bytes(1, "little"))
  output.write(((instrument.detune << 4) | instrument.riff_speed).to_bytes(1, "little"))
  output.write(instrument.volume.to_bytes(1, "little"))

  for op in instrument.operators:
    gen_fm_operator(output, op)



def gen_fm_operator(output, op):
  output.write(((TREMOLO_BIT if op.tremolo else 0) \
    | (VIBRATO_BIT if op.vibrato else 0) \
    | (SUSTAIN_BIT if op.sustain else 0) \
    | op.multiplier).to_bytes(1, "little"))

  output.write(((op.scale_level << 4) | op.volume).to_bytes(1, "little"))
  output.write(((op.adsr_attack << 4) | op.adsr_decay).to_bytes(1, "little"))
  output.write(((op.adsr_sustain << 4) | op.adsr_release).to_bytes(1, "little"))
  output.write(op.waveform.value.to_bytes(1, "little"))


def gen_midi_instrument(output, instrument):
  # TODO
  pass


def gen_orderlist(output, tune):
  output.write(len(tune.order).to_bytes(1, "little"))
  output.write(bytearray(tune.order))


def gen_patterns(output, tune):
  for num, pat in tune.patterns.items():
    pattern = encode_pattern(pat)


def encode_pattern(pattern):
  
