from .algorithm import Algorithm
from .waveform import Waveform
from .pattern import PatternBase

class Instrument:
  """Holds one RAD2 tune instrument"""

  def __init__(self):
    self.number = 0
    self.name   = ""
    self.riff   = None


class FMOperator:
  """Holds the settings of one FM operator"""

  def __init__(self):
    self.tremolo      = False
    self.vibrato      = False
    self.sustain      = False
    self.scale_rate   = False
    self.multiplier   = 1
    self.scale_level  = 1
    self.volume       = 0
    self.adsr_attack  = 0
    self.adsr_decay   = 0
    self.adsr_sustain = 0
    self.adsr_release = 0
    self.waveform     = Waveform.SINE


class FMInstrument(Instrument):
  """Definition of a (normal) OPL FM instrument"""

  def __init__(self):
    super().__init__()
    self.algorithm  = Algorithm.FM
    self.panning12  = 0
    self.panning34  = 0
    self.feedback12 = 0
    self.feedback34 = 0
    self.detune     = 0
    self.riff_speed = 0
    self.volume     = 0
    self.operators  = [FMOperator()] * 4


class MidiInstrument(Instrument):
  """Definition of a MIDI instrument"""

  def __init__(self):
    super().__init__()
    self.port    = 0
    self.channel = 0
    self.version = 0
    self.octave  = 0
    self.program = 0
    self.bank    = 0
