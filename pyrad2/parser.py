from .tune import Tune
from .instrument import FMInstrument, MidiInstrument, FMOperator
from .algorithm import Algorithm
from .waveform import Waveform
from .pattern import Riff, Pattern, PatternLine, PatternLineChannel, InstrumentRiff

from .consts import *

class FormatError(Exception):
  pass


class UnsupportedVersion(Exception):
  pass


def parse(data):
  signature = data.read(16)
  if signature != b"RAD by REALiTY!!":
    raise FormatError()

  file_version = data.read(1)[0]

  if file_version != 0x21:
    raise UnsupportedVersion()

  return parse_rad_file(data)


def parse_rad_file(data):
  timing = data.read(1)[0]

  tune = Tune()
  tune.slow = (timing & SLOW_TUNE_BIT) != 0

  tune.speed = timing & SPEED_MASK

  if (timing & BPM_PRESENT_BIT) != 0:
    tune.bpm = int.from_bytes(data.read(2), "little")

  tune.description = parse_description(data)

  tune.instruments = parse_instruments(data)

  tune.order    = parse_order_list(data)
  tune.patterns = parse_patterns(data)
  tune.riffs    = parse_riffs(data)

  return tune


def parse_description(data):
  byte = data.read(1)[0]
  descr = bytearray()
  while byte != 0:
    if byte == 1:
      descr.append(ord('\n'))
    elif byte < 0x20:
      descr = descr + b' ' * byte
    else:
      descr.append(byte)

    byte = data.read(1)[0]

  return descr.decode()


def parse_instruments(data):
  instruments = {}

  num, instrument = parse_instrument(data)
  while instrument != None:
    instruments[num] = instrument
    num, instrument = parse_instrument(data)

  return instruments



def parse_instrument(data):
  num = data.read(1)[0]

  if num == 0:
    return None, None

  name_len = data.read(1)[0]
  name = data.read(name_len).decode()

  byte0 = data.read(1)[0]


  instrument = None
  if Algorithm(byte0 & ALGORITHM_MASK) == Algorithm.MIDI:
    instrument = parse_midi_instrument(byte0, data)
  else:
    instrument = parse_fm_instrument(byte0, data)

  instrument.name = name


  if (byte0 & HAS_RIFF_BIT) != 0:
    instrument.riff = parse_instrument_riff(data)

  return num, instrument


def extract_bits(byte, shift, count):
  return (byte >> shift) & ((1 << count) - 1)


def parse_fm_instrument(byte0, data):
  instrument = FMInstrument()

  instrument.algorithm = Algorithm(byte0 & ALGORITHM_MASK)
  instrument.panning12 = extract_bits(byte0, 3, 2)
  instrument.panning34 = extract_bits(byte0, 5, 2)

  bytes13 = data.read(3)
  instrument.feedback12 = extract_bits(bytes13[0], 0, 4)
  instrument.feedback34 = extract_bits(bytes13[0], 4, 4)
  instrument.detune     = extract_bits(bytes13[1], 4, 4)
  instrument.riff_speed = extract_bits(bytes13[1], 0, 4)
  instrument.volume     = extract_bits(bytes13[2], 0, 6)

  instrument.operators = [parse_fm_operator(data) for x in range(4)]

  return instrument


def parse_fm_operator(data):

  byte04 = data.read(5)

  op = FMOperator()
  op.tremolo      = (byte04[0] & TREMOLO_BIT) != 0
  op.vibrate      = (byte04[0] & VIBRATO_BIT) != 0
  op.sustain      = (byte04[0] & SUSTAIN_BIT) != 0
  op.scale_rate   = (byte04[0] & SCALE_RATE_BIT) != 0
  op.multiplier   = extract_bits(byte04[0], 0, 4)
  op.scale_level  = extract_bits(byte04[1], 6, 2)
  op.volume       = extract_bits(byte04[1], 0, 6)
  op.adsr_attack  = extract_bits(byte04[2], 4, 4)
  op.adsr_decay   = extract_bits(byte04[2], 0, 4)
  op.adsr_sustain = extract_bits(byte04[3], 4, 4)
  op.adsr_release = extract_bits(byte04[3], 0, 4)
  op.waveform     = Waveform(extract_bits(byte04[4], 0, 3))

  return op


def parse_instrument_riff(data):
  byte_count = int.from_bytes(data.read(2), "little")

  riff = InstrumentRiff()
  riff.lines = parse_pattern_lines(data)

  return riff



def parse_order_list(data):
  length = data.read(1)[0]
  return [int(x) for x in data.read(length)]


def parse_pattern_lines(data):
  last_line = False

  lines = {}

  while not last_line:
    line_no   = data.read(1)[0]
    last_line = (line_no & LAST_ENTRY_BIT) != 0

    line = PatternLine()

    line.channels = parse_pattern_line_channels(data)

    lines[line_no & ~LAST_ENTRY_BIT] = line

  return lines


def parse_pattern_line_channels(data):
  last_channel = False

  channels = {}

  while not last_channel:
    fields = data.read(1)[0]
    last_channel = (fields & LAST_ENTRY_BIT) != 0

    chan = PatternLineChannel()

    if (fields & NOTE_OCTAVE_PRESENT) != 0:
      note_octave = data.read(1)[0]
      chan.note             = extract_bits(note_octave, 0, 4)
      chan.octave           = extract_bits(note_octave, 4, 3)
      chan.reuse_instrument = (note_octave & REUSE_INSTRUMENT_BIT) != 0

    if (fields & INSTRUMENT_PRESENT) != 0:
      chan.instrument = extract_bits(data.read(1)[0], 0, 7)

    if (fields & EFFECT_PARAMETER_PRESENT) != 0:
      chan.effect           = extract_bits(data.read(1)[0], 0, 5)
      chan.effect_parameter = extract_bits(data.read(1)[0], 0, 7)

    channels[fields & CHANNEL_MASK] = chan

  return channels


def parse_patterns(data):
  patterns = {}

  num = data.read(1)[0]
  while num != NO_MORE_PATTERNS:
    data.read(2) # skip length info

    pattern = Pattern()
    pattern.lines = parse_pattern_lines(data)

    patterns[num] = pattern
    num = data.read(1)[0]

  return patterns


def parse_riffs(data):
  riffs = {}

  num = data.read(1)[0]
  while num != NO_MORE_PATTERNS:
    data.read(2) # skip length info

    riff = Riff()
    track   = extract_bits(num, 4, 4)
    channel = extract_bits(num, 0, 4)
    riff.lines = parse_pattern_lines(data)

    riffs[(track,channel)] = riff
    num = data.read(1)[0]

  return riffs
