class LineExists(Exception):
  pass


class LineChannelExists(Exception):
  pass


class PatternBase:
  """Base for Patterns and Riffs. Allows interacting with the lines they contain"""

  def __init__(self):
    self.lines = {} 


Pattern        = PatternBase
Riff           = PatternBase
InstrumentRiff = PatternBase


class PatternLine:
  """Holds the data for a line in a pattern"""

  def __init__(self):
    self.channels = {}


class PatternLineChannel:
  """Holds the data for one channel in a line of a pattern"""

  def __init__(self):
    self.note             = None
    self.octave           = None
    self.reuse_instrument = False
    self.instrument       = None
    self.effect           = None
    self.effect_parameter = None
