from .instrument import FMInstrument, MidiInstrument

class Tune:
  """Holds all the data of a RAD2 tune"""

  def __init__(self):
    self.slow = False
    self.bpm = 125
    self.speed = 0

    self.description = ""

    self.instruments = {}
    self.order       = []
    self.patterns    = {}
    self.riffs       = {}
