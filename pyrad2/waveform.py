from enum import Enum

class Waveform(Enum):
  """Waveforms available for operators on OPL3"""
  SINE           = 0
  HALF_SINE      = 1
  ABS_SINE       = 2
  PULSE_SINE     = 3
  SINE_EVEN      = 4
  ABS_SINE_EVEN  = 5
  SQUARE         = 6
  DERIVED_SQUARE = 7
