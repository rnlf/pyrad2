import unittest
from pyrad2.bitfield import Bitfield
from io import BytesIO

class TestBitfieldDecode(unittest.TestCase):
  def test_no_fields(self):
    bf = Bitfield(0)
    buf = BytesIO(b'test')
    buf.seek(1)

    result = bf.decode(buf)

    # decoding a bitfield without fields should result in
    # an empty dict
    self.assertEqual(result, {})

    # decoding an empty bitfield must not move the read pointer of
    # the underlying buffer
    self.assertEqual(buf.tell(), 1)

  def test_short_read(self):
    bf = Bitfield(1, { "offset":0, "length":3, "name": "foo" })
    buf = BytesIO(b'')

    # not enough data in input should raise EOFError
    with self.assertRaises(EOFError):
      result = bf.decode(buf)


  def test_simple_cases(self):
    bf = Bitfield(1, { "offset": 0, "length": 8, "name": "foo" })
    buf = BytesIO(b'\xff')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], 0xff)

    bf = Bitfield(2, { "offset": 0, "length": 16, "name": "foo" })
    buf = BytesIO(b'\xff\xaa')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], 0xaaff)

    bf = Bitfield(2, { "offset": 0, "length": 16, "name": "foo", "endian": "big" })
    buf = BytesIO(b'\xff\xaa')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], 0xffaa)

    bf = Bitfield(2, { "offset": 0, "length": 16, "name": "foo", "signed": True })
    buf = BytesIO(b'\xff\xaa')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], -21761)

    bf = Bitfield(2, { "offset": 0, "length": 16, "name": "foo", "endian": "big", "signed": True })
    buf = BytesIO(b'\xff\xaa')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], -86)

    bf = Bitfield(3, { "offset": 0, "length": 24, "name": "foo" })
    buf = BytesIO(b'\xff\xaa\x55')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], 0x55aaff)

    bf = Bitfield(3, { "offset": 0, "length": 24, "name": "foo", "endian": "big" })
    buf = BytesIO(b'\xff\xaa\x55')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], 0xffaa55)

    bf = Bitfield(3, { "offset": 0, "length": 24, "name": "foo", "signed": True })
    buf = BytesIO(b'\xff\xaa\x55')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], -11162881)

    bf = Bitfield(3, { "offset": 0, "length": 24, "name": "foo", "endian": "big", "signed": True })
    buf = BytesIO(b'\xff\xaa\x55')
    res = bf.decode(buf)
    self.assertEqual(res['foo'], -21931)
