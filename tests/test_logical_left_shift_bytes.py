import unittest
from pyrad2.bitfield import logical_left_shift_bytes

class TestLogicalLeftShiftBytes(unittest.TestCase):
  def test_zero_shift(self):
    self.assertEqual(logical_left_shift_bytes(b'1234', 0), b'1234')

  def test_shift_one_byte(self):
    self.assertEqual(logical_left_shift_bytes(b'1234', 8), b'1234\x00')

  def test_shift_one_bit(self):
    self.assertEqual(logical_left_shift_bytes(b'\xff',     1), b'\x01\xFE')
    self.assertEqual(logical_left_shift_bytes(b'\xaa\x55', 1), b'\x01\x54\xAa')

  def test_shift_more(self):
    self.assertEqual(logical_left_shift_bytes(bytes([1,2,3,4]), 10), bytes([0,4,8,12,16,0]))

  def test_big_array(self):
    self.assertEqual(logical_left_shift_bytes(b'\x13' * 100, 9), b'\x00'+b'\x26'*100+b'\x00')

  def test_negative_shift(self):
    with self.assertRaises(ValueError):
      logical_left_shift_bytes(b'\x00', -1)

  def test_zero_shift_truncate(self):
    self.assertEqual(logical_left_shift_bytes(b'1234', 0, truncate=4), b'1234')
    self.assertEqual(logical_left_shift_bytes(b'1234', 0, truncate=3), b'234')
    self.assertEqual(logical_left_shift_bytes(b'1234', 0, truncate=2), b'34')
    self.assertEqual(logical_left_shift_bytes(b'1234', 0, truncate=1), b'4')
    self.assertEqual(logical_left_shift_bytes(b'1234', 0, truncate=0), b'')

  def test_shift_one_byte_truncate(self):
    self.assertEqual(logical_left_shift_bytes(b'1234', 8, truncate=4), b'234\x00')

  def test_shift_one_bit_truncate(self):
    self.assertEqual(logical_left_shift_bytes(b'\xff',     1, truncate=1), b'\xFE')
    self.assertEqual(logical_left_shift_bytes(b'\xaa\x55', 1, truncate=3), b'\x01\x54\xAa')


  def test_truncate_keep_original_size(self):
    self.assertEqual(logical_left_shift_bytes(b'\xff\xff\xff\xff', 24, truncate=-1), b'\xff\0\0\0');
    self.assertEqual(logical_left_shift_bytes(b'\xff'*1000, 24, truncate=-1), b'\xff'*997+b'\0\0\0');

