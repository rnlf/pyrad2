import unittest
from pyrad2.bitfield import logical_right_shift_bytes

class TestLogicalRightShiftBytes(unittest.TestCase):
  def test_zero_shift(self):
    self.assertEqual(logical_right_shift_bytes(b'1234', 0), b'1234')

  def test_shift_one_byte(self):
    self.assertEqual(logical_right_shift_bytes(b'1234', 8), b'123')

  def test_shift_one_bit(self):
    self.assertEqual(logical_right_shift_bytes(b'\xff',     1), b'\x7f')
    self.assertEqual(logical_right_shift_bytes(b'\xaa\x55', 1), b'\x55\x2A')

  def test_shift_more(self):
    self.assertEqual(logical_right_shift_bytes(bytes([1,2,3,4]), 10), bytes([0, 0x40, 0x80]))

  def test_shift_out_all(self):
    self.assertEqual(logical_right_shift_bytes(bytes([1,2,3,4]), 32), b'')

  def test_big_array(self):
    self.assertEqual(logical_right_shift_bytes(b'\x13' * 100, 99*8+1), b'\x09')

  def test_negative_shift(self):
    with self.assertRaises(ValueError):
      logical_right_shift_bytes(b'\x00', -1)
